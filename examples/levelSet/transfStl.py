# exec(open('/home/jeduriez/Yade/trunkMine/examples/levelSet/transfStl.py').read())
def transfStl(stlFileName,verbose=True):
    '''Maps a stl mesh into one possible inertial frame. The new .stl will be saved, appending 'Trans' to *stlFileName*
    :stlFileName: path to the .stl to consider, wo the .stl extension (string)
    :verbose: boolean to control message outputs
    '''
    import numpy,time
    import stl # = https://pypi.org/project/numpy-stl/. Alternate choice https://python-stl.readthedocs.io/en/latest/ is no longer maintained on https://github.com/apparentlymart/python-stl

    t1 = time.time()
    stlMesh = stl.mesh.Mesh.from_file(stlFileName+'.stl')
    t2 = time.time()
    if verbose: print('\nReading given file in',t2-t1,'seconds')
    volume, cog, inertia = stlMesh.get_mass_properties() # cog being a numpy array
    t1 = time.time()
    if verbose:
        print('Computing mass properties in ~',t1-t2,'seconds')
        print('Initially, stl is centered at',cog,'with inertia\n',inertia)

    # Getting eigenbasis information:
    [val,vect] = numpy.linalg.eig(inertia) # vect is the eigenvectors basis in current basis
    if numpy.linalg.det(vect) < 0: # eg (x,z,y)
        if verbose:
            print('Non right-handed basis, modifying vect, which is for now\n',vect)
        vect[:,2] = - vect[:,2] # making it (x,z,-y) which is a more suitable basis
        if verbose:
            print('And now\n',vect,'\nwith determinant',numpy.linalg.det(vect))
    rot = vect.T # taking the transpose that will be useful for changing basis
    # Applying the transformation, avoiding to use eg stlMesh.transform or stlMesh.rotate on purpose (with source code at https://github.com/WoLpH/numpy-stl/tree/develop/stl/base.py) because of https://github.com/WoLpH/numpy-stl/issues/166:
    for fIdx in range(len(stlMesh.vectors)): # loop over facets
        for vIdx in range(3): # loop over 3 vertices in each facet
            stlMesh.vectors[fIdx][vIdx] = rot@(stlMesh.vectors[fIdx][vIdx] - cog)
    volume, cog, inertia = stlMesh.get_mass_properties()
    if verbose:
        print('\nAfter transformation, stl is centered at',cog,'and has following inertia\n',inertia)
    stlMesh.save(stlFileName+'Transf.stl')


