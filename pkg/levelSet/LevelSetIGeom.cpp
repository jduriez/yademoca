/*************************************************************************
*  2021 jerome.duriez@inrae.fr                                           *
*  This program is free software, see file LICENSE for details.          *
*************************************************************************/

#ifdef YADE_LS_DEM
#include <pkg/levelSet/LevelSetIGeom.hpp>

namespace yade {
YADE_PLUGIN((MultiScGeom)(LSnodeGeom));

std::vector<Vector3r> MultiScGeom::getContactPts() const{
//	return getContactsInfo<Vector3r,&GenericSpheresContact::contactPoint>();
	std::vector<Vector3r> ret;
	for(unsigned int idx = 0; idx < contacts.size(); idx++)	ret.push_back(contacts[idx]->contactPoint);
	return ret;
}

std::vector<Vector3r> MultiScGeom::getNormals() const{
	std::vector<Vector3r> ret;
	for(unsigned int idx = 0; idx < contacts.size(); idx++)	ret.push_back(contacts[idx]->normal);
	return ret;
}

std::vector<Real> MultiScGeom::getOverlaps() const{
	std::vector<Real> ret;
	for(unsigned int idx = 0; idx < contacts.size(); idx++){
		ret.push_back(contacts[idx]->penetrationDepth);
	}
	return ret;
}

} // namespace yade
#endif //YADE_LS_DEM
