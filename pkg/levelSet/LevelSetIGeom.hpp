/*************************************************************************
*  2021 jerome.duriez@inrae.fr                                           *
*  This program is free software, see file LICENSE for details.          *
*************************************************************************/

#ifdef YADE_LS_DEM
#pragma once
#include <core/IGeom.hpp>
#include <pkg/dem/ScGeom.hpp>

namespace yade {

class MultiScGeom : public IGeom
{
private:
//	template<typename typeOfProp, typeOfProp GenericSpheresContact::* prop> std::vector<typeOfProp> getContactsInfo();
	std::vector<Vector3r> getContactPts() const;
	std::vector<Vector3r> getNormals() const;
	std::vector<Real> getOverlaps() const;
public:
	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR_PY(MultiScGeom,IGeom,"A set of :yref:`ScGeom` for describing the kinematics of an interaction with multiple contact points between two :yref:`LevelSet` bodies. To combine with :yref:`MultiFrictPhys` and associated classes.",
	((vector< shared_ptr<ScGeom> >,contacts,,,"List of :yref:`ScGeom`. Not convertible to Python at the moment, see get* functions to explore contact properties"))
	((vector< int >,nodesIds,,,"List of :yref:`surface nodes<LevelSet.surfaceNodes>` (on id1 if that body is strictly bigger in volume, or id2 otherwise) making :yref:`contacts<MultiScGeom.contacts>`. Contact point for a node of index nodesIds[i] has kinematic properties stored in contacts[i]. Should be equal to :yref:`MultiFrictPhys.nodesIds` by design")) // do we need both ?"
	, // existing Law2::go() will have to apply on a reference but contacts itself can not be a vector of references: https://stackoverflow.com/questions/922360/why-cant-i-make-a-vector-of-references
	createIndex(); // this class will enter InteractionLoop dispatch, we need a create_index() here, and a REGISTER_*_INDEX below (https://yade-dem.org/doc/prog.html#indexing-dispatch-types)
	,.def("getContactPts",&MultiScGeom::getContactPts,"Returns the list of positions of :yref:`contact points<ScGeom.contactPoint>`")
	.def("getNormals",&MultiScGeom::getNormals,"Returns the list of :yref:`normals<ScGeom.normal>`")
	.def("getOverlaps",&MultiScGeom::getOverlaps,"Returns the list of :yref:`overlaps<ScGeom.penetrationDepth>`")
	);
	// clang-format on
	DECLARE_LOGGER;
	REGISTER_CLASS_INDEX(MultiScGeom,IGeom); // see createIndex() remark
};
REGISTER_SERIALIZABLE(MultiScGeom);

class LSnodeGeom : public ScGeom
{
public:
	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(LSnodeGeom,ScGeom,"Extends :yref:`ScGeom` for LevelSet simulations of convex bodies while storing previous node establishing contact, for an optimal contact detection.",
		((int,surfNodeIdx,-1,Attr::readonly,"Index (within :yref:`surfNodes<LevelSet.surfNodes>` of id2 if Volume(id1) > Volume(id2); else id1) of the surface node that currently carries contact"))
		((Real,distWithPast,-1,Attr::readonly,"Distance between the contact-responsible surface nodes at the past and current iterations. (Equal to -1 if there was no contacting node in the past)"))
		,
		createIndex(); // same remark as MultiScGeom
	);
	// clang-format on
	REGISTER_CLASS_INDEX(LSnodeGeom,ScGeom); // see createIndex() remark
};
REGISTER_SERIALIZABLE(LSnodeGeom);

} // namespace yade
#endif // YADE_LS_DEM
