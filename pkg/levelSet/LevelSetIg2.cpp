/*************************************************************************
*  2021 jerome.duriez@inrae.fr                                           *
*  This program is free software, see file LICENSE for details.          *
*************************************************************************/

#ifdef YADE_LS_DEM
#include <pkg/levelSet/LevelSetIGeom.hpp>
#include <pkg/levelSet/LevelSetIg2.hpp>
#include <pkg/levelSet/OtherClassesForLSContact.hpp>
#include <pkg/levelSet/ShopLS.hpp>
#include <preprocessing/dem/Shop.hpp>

namespace yade {
YADE_PLUGIN(
        (Ig2_Box_LevelSet_ScGeom)(Ig2_LevelSet_LevelSet_ScGeom)(Ig2_LevelSet_LevelSet_LSnodeGeom)(Ig2_Wall_LevelSet_ScGeom)(Ig2_LevelSet_LevelSet_MultiScGeom));
CREATE_LOGGER(Ig2_Box_LevelSet_ScGeom);
CREATE_LOGGER(Ig2_LevelSet_LevelSet_LSnodeGeom);
CREATE_LOGGER(Ig2_LevelSet_LevelSet_ScGeom);
CREATE_LOGGER(Ig2_LevelSet_LevelSet_MultiScGeom);
CREATE_LOGGER(Ig2_Wall_LevelSet_ScGeom);

bool Ig2_Box_LevelSet_ScGeom::go(
        const shared_ptr<Shape>& shape1,
        const shared_ptr<Shape>& shape2,
        const State&             state1,
        const State&             state2,
        const Vector3r&          shift2,
        const bool&              force,
        const shared_ptr<Interaction>& c)
{
	// 1.1 Preliminary declarations
	shared_ptr<Box>      boxSh = YADE_PTR_CAST<Box>(shape1);
	shared_ptr<LevelSet> lsSh  = YADE_PTR_CAST<LevelSet>(shape2);
	Vector3r             lsCenter(state2.pos+shift2), boxCenter(state1.pos);
	Vector3r             axisContact(Vector3r::Zero()); // I will need to .dot this vector with Vector3r => Vector3r (and not Vector3i) as well

	// 1.2 Checking the "contact direction", adopting the two following hypothesis:
	// - H1: lsCenter is outside of the box
	// - H2: projecting lsCenter along the contact direction towards the box will make lsCenter fall onto the  box, and not alongside. This is not always fulfilled, but will always be during triaxial box setups, for instance (as long as the box is closed).
	for (int axis = 0; axis < 3; axis++) {
		if (math::abs(lsCenter[axis] - boxCenter[axis]) > boxSh->extents[axis]) // TODO: account for a change in orientation of the box ?
			axisContact[axis] = 1; // TODO: once I'm sure this will happen only once, I could stop the loop here...
	}
	if (axisContact.norm() != 1) {
		LOG_ERROR(
		        "Problem while determining contact direction for "
		        << c->id1 << "-" << c->id2 << " : we got " << axisContact
		        << ". (0 0 0) means the LevelSet'd body has its center inside the box, which is not supported. Indeed, center = " << lsCenter
		        << " while boxCenter = " << boxCenter << " and extents = " << boxSh->extents);
	}
	LOG_DEBUG("axisContact = " << axisContact);
	Real boxC(axisContact.dot(boxCenter)), boxE(boxSh->extents.dot(axisContact)), lsC(lsCenter.dot(axisContact));

	// 2.1. Preliminary declarations for the surface nodes loop
	// clang-format off
	const int nNodes(lsSh->surfNodes.size());
	if (!nNodes) LOG_ERROR("We have one level-set body without boundary nodes for contact detection. Will probably crash");
	vector<Vector3r> lsNodes; // current positions of the boundary nodes (some of of those, at least) for the level set body. See for loop below
	lsNodes.reserve(nNodes); // nNodes will be a maximum size, reserve() is appropriate, not resize() (see also https://github.com/isocpp/CppCoreGuidelines/issues/493)
	vector<Real> distList; // will include all distance values from the node(s)On1 to shape2. It is a std::vector because we do not know beforehand the number of elements in this "list
	distList.reserve(nNodes); // nNodes might be a maximum size, reserve() is appropriate, not resize() (see also https://github.com/isocpp/CppCoreGuidelines/issues/493)
	vector<int> indicesNodes; // distList will include distance values corresponding to nodes e.g. 2, 5, 7 only out of 10 nodes among surfNodes. This indicesNodes vector will store these 2,5,7 indices
	indicesNodes.reserve(nNodes);
	// NB: it might actually be somewhat faster to not use these vectors and just compare new node with previous node, as done in Ig2_LevelSet_LevelSet*
	// I do not think it is critical for present Ig2_Box_LevelSet_ScGeom
	Real distToNode; // one distance value, for one node
	Real xNode;      // current position (on the axisContact of interest, can be something else than x-axis..) of the level set boundary node
	// clang-format on

	// 2.2. Actual loop over surface nodes
	for (int node = 0; node < nNodes; node++) {
		lsNodes[node] = ShopLS::rigidMapping(lsSh->surfNodes[node], Vector3r::Zero(), lsCenter, state2.ori);
		xNode         = lsNodes[node].dot(axisContact);
		if (xNode < boxC - boxE || xNode > boxC + boxE) continue;
		distToNode = (lsC > boxC ? boxC + boxE - xNode : xNode - (boxC - boxE));
		if (distToNode < 0) LOG_ERROR("Unexpected case ! We obtained " << distToNode << " while waiting for a positive quantity");
		distList.push_back(distToNode);
		indicesNodes.push_back(node);
	}

	// 2.3. Finishing the work when there is no contact
	if (!distList.size()) { // all boundary nodes are outside the bounds' overlap,
		if (!c->isReal()) return false;
		else {
			Ig2_LevelSet_LevelSet_ScGeom::geomPtrForLaterRemoval(state1, state2, c, scene);
			return true;
		}
	}
	Real maxOverlap;
	maxOverlap = *std::max_element(distList.begin(), distList.end());
	if (maxOverlap < 0 && !c->isReal() && !force) // inspired by Ig2_Sphere_Sphere_ScGeom:
		return false;

	// 2.4. Finishing the work when there is a contact
	int indexCont = std::min_element(distList.begin(), distList.end())
	        - distList.begin();                             //this works: it seems min_element is returning here a random access iterator
	Vector3r normal((lsC > boxC ? 1. : -1.) * axisContact); // normal from the box body to the level set body, ie from 1 to 2, as expected.
	Real     rad((lsNodes[indicesNodes[indexCont]] - lsCenter).norm());
	shared_ptr<ScGeom> geomPtr;
	bool               isNew = !c->geom;
	if (isNew) {
		geomPtr = shared_ptr<ScGeom>(new ScGeom());
		c->geom = geomPtr;
	} else
		geomPtr = YADE_PTR_CAST<ScGeom>(c->geom);
	Ig2_LevelSet_LevelSet_ScGeom::fillScGeomPtr<ScGeom>(
	        geomPtr,
	        lsNodes[indicesNodes[indexCont]] + maxOverlap / 2. * normal, // middle of overlapping volumes, as usual
	        maxOverlap,                                                  // does not work for very big/huge overlap
	        rad,
	        rad,
	        state1,
	        state2,
	        scene,
	        c,
	        normal,
	        shift2,
	        isNew);
	return true;
}

bool Ig2_Wall_LevelSet_ScGeom::go(
        const shared_ptr<Shape>& shape1,
        const shared_ptr<Shape>& shape2,
        const State&             state1,
        const State&             state2,
        const Vector3r&          shift2,
        const bool&              force,
        const shared_ptr<Interaction>& c)
{
	shared_ptr<Wall>     wallSh = YADE_PTR_CAST<Wall>(shape1);
	shared_ptr<LevelSet> lsSh   = YADE_PTR_CAST<LevelSet>(shape2);
	Real                 lsPos(state2.pos[wallSh->axis]+shift2[wallSh->axis]), wallPos(state1.pos[wallSh->axis]);

	const int nNodes(lsSh->surfNodes.size());
	if (!nNodes) LOG_ERROR("We have one level-set body without boundary nodes for contact detection. Will probably crash");

	Real distToNode, // one wall-level set distance value (< 0 when contact), for one node
	        prevDistToNode(std::numeric_limits<Real>::infinity()),
	        nodePos,        // current position along the Wall->axis of one given boundary node
	        maxOverlap(-1); // TODO: maxOverlap actually redundant with prevDistToNode
	Vector3r currNode,      // current position of one given boundary node
	        contactNode;    // the boundary node which is the most inside the wall
	for (int node = 0; node < nNodes; node++) {
		currNode = ShopLS::rigidMapping(lsSh->surfNodes[node], Vector3r::Zero(), state2.pos+shift2, state2.ori);
		nodePos  = currNode[wallSh->axis];
		if (wallPos >= lsPos && wallPos <= nodePos) // first possibility for the wall to intersect the LevelSet body
			distToNode = wallPos - nodePos;
		else if (wallPos >= nodePos && wallPos <= lsPos) // second possibility for intersection
			distToNode = nodePos - wallPos;
		else
			continue; // go directly to next node
		if (distToNode > 0) LOG_ERROR("Unexpected case ! We obtained " << distToNode << " while waiting for a negative quantity");
		if (distToNode < prevDistToNode) {
			maxOverlap     = -distToNode;
			contactNode    = currNode;
			prevDistToNode = distToNode;
		}
	}
	if (maxOverlap < 0 && !c->isReal() && !force)
		return false; // we won't create the interaction in this case (but it is not our job here to delete it in case it already exists)
	Vector3r wallNormal(Vector3r::Zero()), normal(Vector3r::Zero());
	if (wallSh->axis == 0) wallNormal = Vector3r::UnitX();
	else if (wallSh->axis == 1)
		wallNormal = Vector3r::UnitY();
	else if (wallSh->axis == 2)
		wallNormal = Vector3r::UnitZ();
	normal = (wallPos - lsPos > 0 ? -1 : 1) * wallNormal; // Points from wall to particle center
	Real rad( (contactNode - state2.pos - shift2).norm() ); // Distance from surface to center of level-set body

	shared_ptr<ScGeom> geomPtr;
	bool               isNew = !c->geom;
	if (isNew) {
		geomPtr = shared_ptr<ScGeom>(new ScGeom());
		c->geom = geomPtr;
	} else
		geomPtr = YADE_PTR_CAST<ScGeom>(c->geom);
	Ig2_LevelSet_LevelSet_ScGeom::fillScGeomPtr<ScGeom>(
	        geomPtr,
	        contactNode + maxOverlap / 2. * normal, // middle of overlapping volumes, as usual
	        maxOverlap,                             // does not work for very big/huge overlap
	        rad, // considering the 2* feature of radius* (see comments in Ig2_LevelSet_LevelSet_ScGeom::fillScGeomPtr), this is what makes most sense ?
	        rad, // we keep in particular radius1/2 slightly greater than (contactPoint-center).norm. And we just use the same radii for the two particles, as in Ig2_Box_Sphere_ScGeom
	        state1,
	        state2,
	        scene,
	        c,
	        normal,
	        shift2,
	        isNew);
	return true;
}

void Ig2_LevelSet_LevelSet_ScGeom::geomPtrForLaterRemoval(const State& rbp1, const State& rbp2, const shared_ptr<Interaction>& c, const Scene* scene)
{
	// to use when we can not really compute anything, e.g. bodies lsGrid do not overlap anymore, but still need to have some geom data (while returning true as per general InteractionLoop workflow because it is an existing interaction. Otherwise we would need to update InteractionLoop itself to avoid LOG_WARN messages). Data mostly include an infinite tensile stretch to insure subsequent interaction removal (by Law2)
	// this function is only applied onto a real interaction c, i.e. with an existing geom
//	shared_ptr<ScGeom> geomPtr(YADE_PTR_CAST<ScGeom>(c->geom));
//	bool               isNew = !c->geom;
//	if (isNew) geomPtr = shared_ptr<ScGeom>(new ScGeom());
//	else
//		geomPtr = YADE_PTR_CAST<ScGeom>(c->geom);
	fillScGeomPtr<ScGeom>(YADE_PTR_CAST<ScGeom>(c->geom),
	        Vector3r::Zero() /* inconsequential bullsh..*/,
	        -std::numeric_limits<Real>::infinity() /* arbitrary big tensile value to trigger interaction removal by Law2*/,
	        1, /* inconsequential bullsh..*/
	        1, /* inconsequential bullsh..*/
	        rbp1,
	        rbp2,
		scene,
	        c,
	        Vector3r::UnitX() /* inconsequential bullsh..*/,
		Vector3r::Zero(), /* inconsequential bullsh..*/
	        false /* inconsequential bullsh..*/);
}


// clang-format off
template< class GeomType > void Ig2_LevelSet_LevelSet_ScGeom::fillScGeomPtr(shared_ptr<GeomType> geomPtr,Vector3r ctctPt, Real un, Real rad1, Real rad2, const State& rbp1, const State& rbp2, const Scene* scene, const shared_ptr<Interaction>& c, const Vector3r& currentNormal, const Vector3r& shift2, bool newScGeom){
	// goal is to avoid duplicating the definition of ScGeom attributes and execution of its ::precompute.
	// The ScGeom ptr is passed as attribute to enable a common usage in the *ScGeom (where the existence of c->geom is fully equivalent to the need for creating a new ptr) and *MultiScGeom (where existence of a c->geom tells nothing for the specific ScGeom at hand)
	geomPtr->contactPoint = ctctPt;
	geomPtr->penetrationDepth = un;
	// NB radius1, radius2: those are useful for
	// 1. contact kinematics description if and only if avoidGranularRatcheting, (not the case here)
	// 2. applying contact forces in C-S Law2, if sphericalBodies (not the case here)
	// 3. GSTS time step determination with respect to rotational stiffnesses
	// 4. and also, after being coined as refR1, refR2, for contact stiffness expression in FrictPhys/FrictMat
	geomPtr->radius1 = rad1;
	geomPtr->radius2 = rad2;
	geomPtr->precompute(rbp1,rbp2,scene,c,currentNormal,newScGeom,shift2,false); // the avoidGranularRatcheting=1 expression of relative velocity at contact does not make sense with radius1 and radius2 in all cases of LevelSet-sthg interaction (because the shapes are non-spherical): our present radius1+radius2 may have nothing in common with branch vector
	// precompute will take care of
	// 1. preparing the rotation of shearForce to the new tangent plane (done later, in Law2) defining these orthonormal_axis and twist_axis, if newScGeom = false
	// 2. updating geomPtr->normal (previous value) to currentNormal
	// 3. computing the relative velocity at contact, through getIncidentVel(avoidGranularRatcheting=false), using now-defined contactPoint
	
//	Comparing with Ig2_Sphere_Sphere_ScGeom.cpp, I think everything is here..
}
// clang-format on

void Ig2_LevelSet_LevelSet_LSnodeGeom::fillNodeData(shared_ptr<LSnodeGeom> geom, const shared_ptr<LevelSet>& lsShape, int idxNow)
{
	if (geom->surfNodeIdx >= 0) {
		geom->distWithPast = (lsShape->surfNodes[geom->surfNodeIdx] - lsShape->surfNodes[idxNow]).norm();
	}
	// else doing nothing and distWithPast will stay equal to -1 constructor value
	geom->surfNodeIdx = idxNow;
}

bool Ig2_LevelSet_LevelSet_LSnodeGeom::go(
        const shared_ptr<Shape>& shape1,
        const shared_ptr<Shape>& shape2,
        const State&             state1,
        const State&             state2,
        const Vector3r&          shift2,
        const bool&                    force,
        const shared_ptr<Interaction>& c)
{
	return Ig2_LevelSet_LevelSet_ScGeom::goSingleOrMulti<LSnodeGeom>(true, shape1, shape2, state1, state2, force, c, scene, shift2);
}

bool Ig2_LevelSet_LevelSet_ScGeom::go(
        const shared_ptr<Shape>& shape1,
        const shared_ptr<Shape>& shape2,
        const State&             state1,
        const State&             state2,
        const Vector3r&          shift2,
        const bool&              force,
        const shared_ptr<Interaction>& c)
{
	return goSingleOrMulti<ScGeom>(true, shape1, shape2, state1, state2, force, c, scene, shift2);
}

bool Ig2_LevelSet_LevelSet_MultiScGeom::go(
        const shared_ptr<Shape>& shape1,
        const shared_ptr<Shape>& shape2,
        const State&             state1,
        const State&             state2,
        const Vector3r&          shift2,
        const bool&                    force,
        const shared_ptr<Interaction>& c)
{
	return Ig2_LevelSet_LevelSet_ScGeom::goSingleOrMulti<ScGeom>(
	        false, shape1, shape2, state1, state2, force, c, scene, shift2); // NB: the choice of template parameter value is inconsequential here
}

template <class SingleGeomType>
bool Ig2_LevelSet_LevelSet_ScGeom::goSingleOrMulti(
        bool                           single,
        const shared_ptr<Shape>&       shape1,
        const shared_ptr<Shape>&       shape2,
        const State&                   state1,
        const State&                   state2,
        const bool&                    force,
        const shared_ptr<Interaction>& c,
        Scene*                         scene,
	const Vector3r&                shift2)
{
	// 1. We first determine the Aabb zone where bodies' bounds overlap. TODO: possible use of Eigen AlignedBox ?
	std::array<Real, 6>     overlap; // the xA,xB, yA,yB, zA,zB defining the Aabb where bounds overlap: for x in [xA,xB] ; y in [yA,yB] ; ...
	const shared_ptr<Bound> bound1 = Body::byId(c->id1, scene)->bound;
	const shared_ptr<Bound> bound2 = Body::byId(c->id2, scene)->bound;
	for (int axis = 0; axis < 3; axis++) {
	  overlap[2 * axis]     = math::max(bound1->min[axis], bound2->min[axis]+shift2[axis]);
	  overlap[2 * axis + 1] = math::min(bound1->max[axis], bound2->max[axis]+shift2[axis]);
		if (overlap[2 * axis + 1]
		    < overlap[2 * axis]) { // an empty bound overlap here (= is possible and OK: it happens when bodies' bounds are just tangent)
			if (!c->isReal()) return false;
			else {
				Ig2_LevelSet_LevelSet_ScGeom::geomPtrForLaterRemoval(state1, state2, c, scene);
				return true;
			}
		}
	}
	Vector3r minBoOverlap(Vector3r(overlap[0], overlap[2], overlap[4])),
	        maxBoOverlap(Vector3r(overlap[1], overlap[3], overlap[5])); // current configuration obviously
	// End of overlap computations

	// 2. We go now for the master-slave contact algorithm with surface ie boundary nodes:
	// 2.1 Preliminary declarations:
	bool                       newIgeom(true);
	shared_ptr<SingleGeomType> geomSingle(new SingleGeomType); // useful only if single but let s also make it here in full scope
	shared_ptr<MultiScGeom>    geomMulti(new MultiScGeom);     // useful only if !single but has to be in full scope
	shared_ptr<MultiFrictPhys> physMulti(new MultiFrictPhys);  // useful only if !single but has to be in full scope
	if (c->geom) {
		newIgeom = false;
		if (single)
			geomSingle = YADE_PTR_CAST<SingleGeomType>(c->geom);
		else {
			geomMulti = YADE_PTR_CAST<MultiScGeom>(c->geom);
			physMulti = YADE_PTR_CAST<MultiFrictPhys>(c->phys);
		}
	} // nothing to do in else
	shared_ptr<LevelSet> sh1 = YADE_PTR_CAST<LevelSet>(shape1);
	shared_ptr<LevelSet> sh2 = YADE_PTR_CAST<LevelSet>(shape2);
	bool                 id1isBigger(
                sh1->getVolume() > sh2->getVolume()); // identifying the smallest particle (where the master-slave contact algorithm will locate boundary nodes)
	shared_ptr<LevelSet> shB(id1isBigger ? sh1 : sh2); // the "big" shape
	shared_ptr<LevelSet> shS(id1isBigger ? sh2 : sh1); // the "small" shape
	// centr*ini, centr*end, and rot* below define the bodies' changes in configuration since the beginning of the simulation:
	Vector3r centrSini(Vector3r::Zero()),
	        centrBini(Vector3r::Zero()); // with a correct ls body creation and use, does not matter whether we take sh1->getCenter or 0
	Vector3r    centrSend(id1isBigger ? (state2.pos+shift2) : state1.pos), centrBend(id1isBigger ? state1.pos : (state2.pos+shift2));
	Quaternionr rotS(id1isBigger ? state2.ori : state1.ori),                    // ori = rotation from reference configuration (local axes) to current one
	        rotB(id1isBigger ? state1.ori.conjugate() : state2.ori.conjugate()) // ori.conjugate() from the current configuration to the reference one
	        ;
	const int nNodes(shS->surfNodes.size());
	if (!nNodes) LOG_ERROR("We have one level-set body without boundary nodes for contact detection. Will probably crash");
	Real distToNode, // one distance value, for one node
	        minusMaxOverlap(std::numeric_limits<Real>::infinity());              // useful only if single but has to be in full scope
	Vector3r minLSgrid(shB->lsGrid->min), maxLSgrid(shB->lsGrid->max()), nodeOfS // some node of smaller Body, in current configuration
	        ,
	        nodeOfSinB // mapped into initial configuration of larger Body
	        ,
	        normal, contactNode;
	int surfNodeIdx(-1);

	// 2.2 Actual loop over surface nodes:
	for (int node = 0; node < nNodes; node++) {
		nodeOfS = ShopLS::rigidMapping(shS->surfNodes[node], centrSini, centrSend, rotS); // current position of this boundary node
		if (!Shop::isInBB(nodeOfS, minBoOverlap, maxBoOverlap)) continue;
		nodeOfSinB = ShopLS::rigidMapping(nodeOfS, centrBend, centrBini, rotB); // mapping this node into the big Body's local frame
		if (!Shop::isInBB(
		            nodeOfSinB,
		            minLSgrid,
		            maxLSgrid)) // possible when bodies (and their shape.corners) rotate, leading their bounds to possibly "inflate" (think of a sphere)
			continue;
		distToNode = shB->distance(nodeOfSinB);
		if (single) {
			if (distToNode < 0 and distToNode < minusMaxOverlap) { // when single, only greatest penetration depth is relevant
				surfNodeIdx = node; // storing the idx of that node. The rest (like computing normal) will be done once for all outside the loop
				minusMaxOverlap = distToNode; // also storing just corresponding overlap
			}
		} else { // !single
			if (distToNode < 0) {
				normal = rotS * shS->normal(shS->surfNodes[node]); // getting the normal as above (single)
				if (id1isBigger)
					normal *= -1;  // as above (single)
				contactNode = nodeOfS; // as above (single)
				const auto findIt(std::find(
				        geomMulti->nodesIds.begin(),
				        geomMulti->nodesIds.end(),
				        node));                            // no problem even if nodesIds is empty eg newIgeom = true
				if (findIt != geomMulti->nodesIds.end()) { // that node was already contacting before
					// we update the geom:
					fillScGeomPtr<ScGeom>(
					        geomMulti->contacts[std::distance(geomMulti->nodesIds.begin(), findIt)],
					        contactNode + (id1isBigger ? -1 : 1) * distToNode / 2. * normal // middle of overlapping volumes, as usual
					        ,
					        -distToNode // does not work for very big/huge overlap, eg when one sphere's center gets into another.
					        ,
					        (contactNode - state1.pos).norm(), //TODO PBC
					        (contactNode - state2.pos).norm(), //TODO PBC
					        state1,
					        state2,
					        scene,
					        c,
					        normal,
						shift2,
					        false);
					// and have nothing to do for what concerns the phys
				} else { // that node was not contacting before
					// we store the information of contact for that node in Multi* geom and phys:
					geomMulti->nodesIds.push_back(node);
					physMulti->nodesIds.push_back(node);
					// we then create a new SingleGeomType shared_ptr:
					shared_ptr<SingleGeomType> scGeomPtr(new SingleGeomType);
					// filled with appropriate data:
					fillScGeomPtr<ScGeom>(
					        scGeomPtr,
					        contactNode + (id1isBigger ? -1 : 1) * distToNode / 2. * normal // middle of overlapping volumes, as usual
					        ,
					        -distToNode // does not work for very big/huge overlap, eg when one sphere's center gets into another.
					        ,
					        (contactNode - state1.pos).norm(),
					        (contactNode - state2.pos).norm(),
					        state1,
					        state2,
					        scene,
					        c,
					        normal,
					        shift2,
						true);
					// that we store in MultiScGeom::contacts:
					geomMulti->contacts.push_back(scGeomPtr);
					// we also have to create a new FrictPhys shared_ptr:
					shared_ptr<FrictPhys> frictPhysPtr(new FrictPhys);
					// with properties (these below lines unfortunately just give 0 at interaction creation ! Because Ip2 could not enter into play yet. It would have helped if Ip2 would be executed *before* Ig2 in InteractionLoop.. Will be corrected in Ip2):
					frictPhysPtr->kn                     = physMulti->kn;
					frictPhysPtr->ks                     = physMulti->ks;
					frictPhysPtr->tangensOfFrictionAngle = std::tan(physMulti->frictAngle);
					// we store in MultiFrictPhys::contacts:
					physMulti->contacts.push_back(frictPhysPtr);
				}
			} else { // distToNode >= 0 and !single: we do not keep this as a contact point (even if distToNode = 0)
				const auto findIt(std::find(geomMulti->nodesIds.begin(), geomMulti->nodesIds.end(), node));
				if (findIt != geomMulti->nodesIds.end()) { // that node was contacting before, we need to remove it
					// we avoid the swap - pop_back method used in FastMarchingMethod since https://stackoverflow.com/a/4442529/9864634 mentions a not understood relation about order of elements (which is important here)
					// erase-remove idiom (https://stackoverflow.com/a/3385251/9864634) to test even though we do not want to remove by value ?
					const auto distanceInItera(std::distance(geomMulti->nodesIds.begin(), findIt));
					geomMulti->contacts.erase(geomMulti->contacts.begin() + distanceInItera);
					geomMulti->nodesIds.erase(findIt);
					physMulti->contacts.erase(physMulti->contacts.begin() + distanceInItera);
					physMulti->nodesIds.erase(physMulti->nodesIds.begin() + distanceInItera);
				} // nothing to do in else (when the node was also not contacting before)
			}
		}
	}
	if (single && surfNodeIdx >= 0) /* 2nd condition checks whether we did detect contact */ {
		normal = rotS
		        * shS->normal(
		                shS->surfNodes
		                        [surfNodeIdx]); // shS->surfNodes[..] (and normal() to itself) refers to the initial shape, current normal is obtained with rotS *. It is for now the outward normal to the small particle
		if (id1isBigger)
			normal *= -1; // if necessary, we make the normal from 1 to 2, as expected
		contactNode = ShopLS::rigidMapping(shS->surfNodes[surfNodeIdx], centrSini, centrSend, rotS); // current position of this boundary node
		if (std::is_same<SingleGeomType, LSnodeGeom>::value) {
			Ig2_LevelSet_LevelSet_LSnodeGeom::fillNodeData(YADE_PTR_CAST<LSnodeGeom>(geomSingle), shS, surfNodeIdx);
		}
	} else
		LOG_DEBUG(
		        "Having here " << geomMulti->contacts.size() << " guys in ig->contacts, vs " << physMulti->contacts.size() << " in ip->contacts\n And"
		                       << geomMulti->nodesIds.size() << " guys in ig->nodesIds, vs " << physMulti->nodesIds.size() << " in ip->nodesIds");

	// 2.3 Finishing the work:
	if (!c->isReal() && !force && ((single && minusMaxOverlap > 0) or (!single && geomMulti->contacts.size() == 0)))
		return false; // we won't create the interaction in this case (but it is not our job here to delete it -- outside returning false unless force, see Ig2_Sphere_Sphere_ScGeom -- in case it already exists)
	if (single) {
		fillScGeomPtr<SingleGeomType>(
		        geomSingle,
		        contactNode + (id1isBigger ? -1 : 1) * minusMaxOverlap / 2. * normal, // middle of overlapping volumes, as usual
		        -minusMaxOverlap, // does not work for very big/huge overlap, eg when one sphere's center gets into another.
		        (contactNode - state1.pos + minusMaxOverlap * (id1isBigger ? normal : Vector3r::Zero()) ).norm(), // radius1 value: taking center to surface distance for id1, expressed from contactNode and possible offset (depending on which particle contactNode came from)
		        (contactNode - state2.pos - shift2 + minusMaxOverlap * (id1isBigger ? Vector3r::Zero() : normal) ).norm(), // radius2: same thing for id2. Reminder: contactNode does belong to id2 if id1 is bigger
		        state1,
		        state2,
		        scene,
		        c,
		        normal,
		        shift2,
			newIgeom);
	}
	if (newIgeom) {
		if (single)
			c->geom = geomSingle;
		else {
			c->geom = geomMulti; // ternary expression with above "= geomSingle" not possible because geomSingle and geomMulti are of different types
			c->phys = physMulti;
		}
	}
	if (!single)
		LOG_DEBUG("Will return true");
	return true;
}
} // namespace yade
#endif //YADE_LS_DEM
