/*************************************************************************
*  2021 jerome.duriez@inrae.fr                                           *
*  This program is free software, see file LICENSE for details.          *
*************************************************************************/

#ifdef YADE_LS_DEM
#pragma once
#include <pkg/dem/ElasticContactLaw.hpp>
#include <pkg/levelSet/LevelSetIGeom.hpp>            // for MultiScGeom
#include <pkg/levelSet/OtherClassesForLSContact.hpp> // for MultiFrictPhys

namespace yade { // Cannot have #include directive inside.

class Law2_MultiScGeom_MultiFrictPhys_CundallStrack : public Law2_ScGeom_FrictPhys_CundallStrack {
public:
	bool go(shared_ptr<IGeom>& _geom, shared_ptr<IPhys>& _phys, Interaction* I) override;
	// clang-format off
		YADE_CLASS_BASE_DOC/*_ATTRS_CTOR_PY*/(Law2_MultiScGeom_MultiFrictPhys_CundallStrack,Law2_ScGeom_FrictPhys_CundallStrack,"Applies :yref:`Law2_ScGeom_FrictPhys_CundallStrack` at each contact point of a (yref:`MultiScGeom`;yref:`MultiFrictPhys`) contact."/*,
		,,
		.def("elasticEnergy",&Law2_ScGeom_FrictPhys_CundallStrack::elasticEnergy,"Compute and return the total elastic energy in all \"FrictPhys\" contacts")
		.def("plasticDissipation",&Law2_ScGeom_FrictPhys_CundallStrack::getPlasticDissipation,"Total energy dissipated in plastic slips at all FrictPhys contacts. Computed only if :yref:`Law2_ScGeom_FrictPhys_CundallStrack::traceEnergy` is true.")
		.def("initPlasticDissipation",&Law2_ScGeom_FrictPhys_CundallStrack::initPlasticDissipation,"Initialize cummulated plastic dissipation to a value (0 by default).")*/
//			DO WE INHERIT THE ABOVE .def methods ? Not so sure ?
	);
	// clang-format on
	FUNCTOR2D(MultiScGeom, MultiFrictPhys);
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Law2_MultiScGeom_MultiFrictPhys_CundallStrack);

} // namespace yade
#endif //YADE_LS_DEM
